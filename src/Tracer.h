#pragma once

#include "glm/glm.hpp"
#include "Types.h"
#include "Scene.h"

#include "string"

class CTracer {
	vec3 Right_HalfPlane;
	vec3 Up_HalfPlane;
public:
	SCamera m_camera;
	CScene* m_pScene;
public:
	SRay MakeRay(unsigned x, unsigned y, unsigned w, unsigned h);  // Create ray for specified pixel
	glm::vec3 TraceRay(SRay ray); // Trace ray, compute its color
	void RenderImage();
	void SaveImageToFile(std::string fileName);
	CTracer(string &file): m_camera(file) {}
};